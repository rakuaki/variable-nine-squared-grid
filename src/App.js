import React, { useEffect } from 'react';
import './App.css';

function App() {
  useEffect(() => {
    var items = document.querySelectorAll('.item');
    var container = document.querySelector('.container');
    items.forEach((item, index) => {
      item.style.backgroundColor = `hsl(${35 + 20 * index}, 70%, 75%)`;
    });
    items.forEach((item, index) => {
      var row = Math.floor(index / 3);
      var col = index % 3;
      var oriRow = ['1fr', '1fr', '1fr'];
      var oriCol = ['1fr', '1fr', '1fr'];
      if (row === 0 || row === 2) {
        oriRow[2 - row] = '0.5fr';
      }
      if (col === 0 || col === 2) {
        oriCol[2 - col] = '0.5fr';
      }
      if (row === 1) {
        oriRow[0] = '0.75fr';
      }
      if (col === 1) {
        oriCol[0] = '0.75fr';
      }
      oriRow[row] = '2.5fr';
      oriCol[col] = '2.5fr';

      item.addEventListener('mouseenter', () => {
        container.style.gridTemplateRows = oriRow.join(' ');
        container.style.gridTemplateColumns = oriCol.join(' ');
      });

      item.addEventListener('mouseleave', () => {
        container.style.gridTemplateRows = '1fr 1fr 1fr';
        container.style.gridTemplateColumns = '1fr 1fr 1fr';
      });
    });
  }, []);

  return (
    <div className="container">
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
      <div className='item'></div>
    </div>
  );
}

export default App;
